import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

// const appRoutes:Routes=[  
//   { path:'', redirectTo: 'home', pathMatch: 'full' },
//   { path: 'home', component: WorkAreaComponent}

// ]

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents:[ ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxQRCodeModule,
    // RouterModule.forRoot(appRoutes, {useHash: false}),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
