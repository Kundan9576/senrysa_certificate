import { Component } from '@angular/core';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import * as saveAs from 'file-saver'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor( ) {
  }

  title = 'Senrysa';
  participantName: string = 'Biswajit Halder';

  elementType = 'url';
  value = 'Techiediaries';
  
  downloadPdf(){
    var data = document.getElementById('container'); 
    html2canvas(data).then(canvas => { 
    // var imgWidth = 208; 
    // var imgHeight = canvas.height * imgWidth / canvas.width;    
    const contentDataURL = canvas.toDataURL('image/png');
    // console.log( contentDataURL ); 
    let pdf = new jsPDF('l', 'mm', 'a4'); // A4 size page of PDF 
    var position = 10; 
    pdf.addImage(contentDataURL, 'PNG', 0, 0, 297, 210)     
    pdf.save('htmltopdf.pdf'); // Generated PDF  
    })
  }

}
